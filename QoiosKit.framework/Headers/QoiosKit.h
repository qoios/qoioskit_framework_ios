//
//  QoiosKit.h
//  QoiosKit
//
//  Created by Fabian Mecklenburg on 17.02.18.
//  Copyright © 2018 Fabian Mecklenburg. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for QoiosKit.
FOUNDATION_EXPORT double QoiosKitVersionNumber;

//! Project version string for QoiosKit.
FOUNDATION_EXPORT const unsigned char QoiosKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <QoiosKit/PublicHeader.h>


