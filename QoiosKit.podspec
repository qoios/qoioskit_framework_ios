

Pod::Spec.new do |s|

  
  s.name            =   "QoiosKit"
  s.version         =   "0.1"
  s.summary         =   "Translate the App from the builder." 
  s.description     =   "QoiosKit translate the output of the builder to an app"
  s.homepage        =   "http://EXAMPLE/QoiosKit"
  s.license         =   { :type => "MIT", :file => "FILE_LICENSE" }
  s.author          =   { "Fabian Mecklenburg" => "fabian.mecklenburg@online.de" }
  s.source          =   { :git => "https://Fabian86@bitbucket.org/qoios/qoioskit_framework_ios.git" }
 # s.source_files    =   "QoiosKit/**/*.{swift}"
  

  s.ios.deployment_target = '10.0'
  s.vendored_frameworks = 'QoiosKit.framework'

  s.requires_arc = true
    s.dependency 'PureLayout', '3.0.2'
    s.dependency 'UIColor_Hex_Swift', '~> 4.0.1'
    s.dependency 'Alamofire', '4.3.0'
    s.dependency 'SwiftyJSON'
    s.dependency 'SDWebImage', '~> 4.0'
    s.dependency 'Bolts-Swift', '1.3.0'
    s.dependency 'BrightFutures'
end
